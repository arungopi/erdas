---
title: Credits
disableToc: true
---

## Contributors

This initiative is based on the practical report submitted by PGDGIST students of batch 2018-19. Suggestions are welcome. Contact: garunalways@disroot.org
