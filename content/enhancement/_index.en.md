---
title: Enhancement Techniques
weight: 15
chapter: true
pre: "<b>2. </b>"
---

### Chapter 3

# Enhancement Techniques


This sections will help you to create  Normalised Difference Vegetation Index(NDVI) and Normalised Difference Built-up Index (NDBI) using Erdas Imagine.
